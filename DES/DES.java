import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public final class DES {

   private Byte[] L, R, C, D, E, S, P, ExorK, roundKey;
   private Byte round;
   private Character mode;

   Byte[] encrypt(Byte[] plaintextBits, Byte[] keyBits) throws UnsupportedEncodingException {
      this.mode = 'e';

      System.out.print("Input:\t");
      print(plaintextBits);
      System.out.print("Key:\t");
      print(keyBits);

      Byte[][] LR = split(IP(plaintextBits));
      this.L = LR[0];
      this.R = LR[1];

      Byte[][] CD = split(PC1(keyBits));
      this.C = CD[0];
      this.D = CD[1];

      System.out.print("CD[0]:\t");
      print(merge(this.C, this.D));

      System.out.print("L[0]:\t");
      print(this.L);

      System.out.print("R[0]:\t");
      print(this.R);

      for (int i = 1; i <= 16; i++) {
         this.round = (byte) i;
         this.generateKey();

         Byte[] Rcopy = this.R;

         this.E = E(this.R);
         this.ExorK = xor(this.E, this.roundKey);
         this.S = S(this.ExorK);
         this.P = P(this.S);

         this.R = xor(this.L, this.P);
         this.L = Rcopy;

         System.out.printf("\n[=== Verschluesselungsrunde %d ===]\n", this.round);
         System.out.print("E:\t");
         print(this.E);

         System.out.printf("k:\t");
         print(this.roundKey);

         System.out.print("E^k:\t");
         print(this.ExorK);

         System.out.print("S-Box:\t");
         print(this.S);

         System.out.print("P:\t");
         print(this.P);

         System.out.print("L:\t");
         print(this.L);

         System.out.print("R:\t");
         print(this.R);
      }

      return PC1inv(merge(this.R, this.L));
   }

   Byte[] decrypt(Byte[] encryptedBits, Byte[] keyBits) throws UnsupportedEncodingException {
      this.mode = 'd';

      System.out.print("Input:\t");
      print(encryptedBits);

      System.out.print("Key:\t");
      print(keyBits);

      Byte[][] IPcipherSplit = split(IP(encryptedBits));
      this.L = IPcipherSplit[0];
      this.R = IPcipherSplit[1];

      Byte[][] CD = split(PC1(keyBits));
      this.C = CD[0];
      this.D = CD[1];

      System.out.print("CD[0]:\t");
      print(merge(this.C, this.D));

      System.out.print("L[0]:\t");
      print(this.L);

      System.out.print("R[0]:\t");
      print(this.R);

      for (int i = 1; i <= 16; i++) {
         this.round = (byte) i;
         this.generateKey();

         Byte[] Rcopy = this.R;

         this.E = E(this.R);
         this.ExorK = xor(this.E, this.roundKey);
         this.S = S(this.ExorK);
         this.P = P(this.S);

         this.R = xor(this.L, this.P);
         this.L = Rcopy;

         System.out.printf("\n[=== Entschluesselungsrunde %d ===]\n", this.round);
         System.out.print("E:\t");
         print(this.E);

         System.out.printf("k:\t");
         print(this.roundKey);

         System.out.print("E^k:\t");
         print(this.ExorK);

         System.out.print("S-Box:\t");
         print(this.S);

         System.out.print("P:\t");
         print(this.P);

         System.out.print("L:\t");
         print(this.L);

         System.out.print("R:\t");
         print(this.R);
      }

      return PC1inv(merge(this.R, this.L));
   }

   private void generateKey() {
      byte n = 0;
      if (this.mode == 'e') {
         byte r = this.round;

         if (r == 1 || r == 2 || r == 9 || r == 16) {
            n = 1;
         } else {
            n = 2;
         }

         this.C = rotate(this.C, n, 'e');
         this.D = rotate(this.D, n, 'e');

         this.roundKey = PC2(merge(this.C, this.D));
      } else if (this.mode == 'd') {
         byte r = this.round;

         if (r == 1) {
            n = 0;
         } else if (r == 2 || r == 9 || r == 16) {
            n = 1;
         } else {
            n = 2;
         }

         this.C = rotate(this.C, n, 'd');
         this.D = rotate(this.D, n, 'd');

         this.roundKey = PC2(merge(this.C, this.D));
      }
   }

   private static Byte[] permute(Byte[] permutation, Byte[] input) {
      int len = permutation.length;
      Byte[] output = new Byte[len];

      for (int i = 0; i < len; i++) {
         output[i] = input[permutation[i] - 1];
      }

      return output;
   }

   private static Byte[] rotate(Byte[] _28BitInput, int n, char mode) {
      if (_28BitInput.length == 28) {
         Byte[] extracted = new Byte[n];
         Byte[] rest = new Byte[28 - n];

         if (mode == 'e') {
            for (int i = 0; i < n; i++) {
               extracted[i] = _28BitInput[i];
            }

            for (int i = n; i < 28; i++) {
               rest[i - n] = _28BitInput[i];
            }

            return merge(rest, extracted);
         } else if (mode == 'd') {
            int index = 0;
            for (int i = 28 - n; i < 28; i++) {
               extracted[index] = _28BitInput[i];
               index++;
            }

            for (int i = 0; i < 28 - n; i++) {
               rest[i] = _28BitInput[i];
            }

            return merge(extracted, rest);
         }

         return null;
      }

      return null;
   }

   private static Byte[] xor(Byte[] a, Byte[] b) {
      if (a.length == b.length) {
         byte len = (byte) a.length;
         Byte[] res = new Byte[len];

         for (int i = 0; i < len; i++) {
            res[i] = (a[i] == b[i]) ? (byte) 0 : (byte) 1;
         }

         return res;
      }

      return null;
   }

   private static Byte[] merge(Byte[] a, Byte[] b) {
      byte aLen, bLen, resLen;
      aLen = (byte) a.length;
      bLen = (byte) b.length;
      resLen = (byte) (aLen + bLen);

      Byte[] res = new Byte[resLen];

      for (int i = 0; i < aLen; i++) {
         res[i] = a[i];
      }

      byte index = 0;
      for (int i = aLen; i < resLen; i++) {
         res[i] = b[index];
         index++;
      }

      return res;
   }

   private static Byte[] stringToByteArray(String str) {
      Byte[] res = new Byte[str.length()];

      for (int i = 0; i < str.length(); i++) {
         res[i] = str.charAt(i) == '1' ? (byte) 1 : (byte) 0;
      }

      return res;
   }

   Byte[] plaintextToBin(String plaintext) throws UnsupportedEncodingException {
      if (plaintext.length() <= 8) {
         byte[] byteChunk = plaintext.getBytes("UTF-8");

         String tmp = "";
         for (int i = 0; i < byteChunk.length; i++) {
            String bin = Integer.toBinaryString(byteChunk[i]);
            while (bin.length() < 8) {
               bin = "0" + bin;
            }

            tmp += bin;
         }

         return stringToByteArray(tmp);
      }

      return null;
   }

   Byte[] keyToBin(String _16asciiInput) throws UnsupportedEncodingException {
      if (_16asciiInput.length() == 16) {
         String bin = Long.toBinaryString(new BigInteger(_16asciiInput, 16).longValue());

         while (bin.length() < 64) {
            bin = "0" + bin;
         }

         return stringToByteArray(bin);
      }

      return null;
   }

   private static Byte binToDec(Byte[] bin) {
      String binStr = "";
      for (Byte c : bin) {
         binStr += Byte.toString(c);
      }

      return (byte) Integer.parseInt(binStr, 2);
   }

   private static String decToBin(Byte dec) {
      return Integer.toString(dec, 2);
   }

   private static Byte[][] split(Byte[] bitstring) {
      byte len = (byte) bitstring.length;
      byte halfLen = (byte) (len / 2);

      Byte[] firstHalf = new Byte[halfLen];
      Byte[] secondHalf = new Byte[halfLen];

      for (int i = 0; i < halfLen; i++) {
         firstHalf[i] = bitstring[i];
      }

      byte index = 0;
      for (int i = halfLen; i < len; i++) {
         secondHalf[index] = bitstring[i];
         index++;
      }

      if (firstHalf.length == secondHalf.length) {
         return new Byte[][] { firstHalf, secondHalf };
      }

      return null;
   }

   private static Byte[] boxSelect(Byte[] _6BitInput, Byte[][] box) {
      Byte[] rowBin = { _6BitInput[0], _6BitInput[5] };
      Byte[] columnBin = new Byte[4];

      for (int i = 1; i < 5; i++) {
         columnBin[i - 1] = _6BitInput[i];
      }

      Byte rowDec = binToDec(rowBin);
      Byte columnDec = binToDec(columnBin);

      String resBin = decToBin(box[rowDec][columnDec]);

      while (resBin.length() < 4) {
         resBin = "0" + resBin;
      }

      return stringToByteArray(resBin);
   }

   private static Byte[] IP(Byte[] _64BitInput) {
      if (_64BitInput.length == 64) {
         Byte[] ip = { 58, 50, 42, 34, 26, 18, 10, 2, 
                       60, 52, 44, 36, 28, 20, 12, 4, 
                       62, 54, 46, 38, 30, 22, 14, 6,
                       64, 56, 48, 40, 32, 24, 16, 8, 
                       57, 49, 41, 33, 25, 17,  9, 1, 
                       59, 51, 43, 35, 27, 19, 11, 3,
                       61, 53, 45, 37, 29, 21, 13, 5,
                       63, 55, 47, 39, 31, 23, 15, 7 };

         return permute(ip, _64BitInput);
      }

      return null;
   }

   private static Byte[] PC1inv(Byte[] _64BitInput) {
      if (_64BitInput.length == 64) {
         Byte[] ipinv = { 40, 8, 48, 16, 56, 24, 64, 32,
                          39, 7, 47, 15, 55, 23, 63, 31,
                          38, 6, 46, 14, 54, 22, 62, 30, 
                          37, 5, 45, 13, 53, 21, 61, 29, 
                          36, 4, 44, 12, 52, 20, 60, 28, 
                          35, 3, 43, 11, 51, 19, 59, 27, 
                          34, 2, 42, 10, 50, 18, 58, 26, 
                          33, 1, 41,  9, 49, 17, 57, 25 };

         return permute(ipinv, _64BitInput);
      }

      return null;
   }

   private static Byte[] E(Byte[] _32BitInput) {
      if (_32BitInput.length == 32) {
         Byte[] e = { 32,  1,  2,  3,  4,  5, 
                       4,  5,  6,  7,  8,  9, 
                       8,  9, 10, 11, 12, 13, 
                      12, 13, 14, 15, 16, 17, 
                      16, 17, 18, 19, 20, 21, 
                      20, 21, 22, 23, 24, 25, 
                      24, 25, 26, 27, 28, 29, 
                      28, 29, 30, 31, 32,  1 };

         return permute(e, _32BitInput);
      }

      return null;
   }

   private static Byte[] S(Byte[] _48BitInput) {
      if (_48BitInput.length == 48) {
         Byte[][] _6BitInput = new Byte[8][6];

         byte x, y;
         x = y = 0;
         for (int i = 0; i < 48; i++) {
            _6BitInput[x][y] = _48BitInput[i];

            if (y == 5) {
               y = 0;
               x++;
            } else {
               y++;
            }
         }

         Byte[][] s1 = { /* S-Box 1 */
               { 14,  4, 13, 1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9, 0,  7 },
               {  0, 15,  7, 4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5, 3,  8 },
               {  4,  1, 14, 8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10, 5,  0 },
               { 15, 12,  8, 2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0, 6, 13 }
         };

         Byte[][] s2 = { /* S-Box 2 */
               { 15,  1,  8, 14,  6, 11,  3,  4,  9, 7,  2, 13, 12, 0,  5, 10 },
               {  3, 13,  4,  7, 15,  2,  8, 14, 12, 0,  1, 10,  6, 9, 11,  5 },
               {  0, 14,  7, 11, 10,  4, 13,  1,  5, 8, 12,  6,  9, 3,  2, 15 },
               { 13,  8, 10,  1,  3, 15,  4,  2, 11, 6,  7, 12,  0, 5, 14,  9 } 
         };

         Byte[][] s3 = { /* S-Box 3 */
               { 10,  0,  9, 14, 6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8 },
               { 13,  7,  0,  9, 3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1 },
               { 13,  6,  4,  9, 8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7 },
               {  1, 10, 13,  0, 6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12 }
         };

         Byte[][] s4 = { /* S-Box 4 */
               {  7, 13, 14, 3,  0,  6,  9, 10,  1, 2, 8,  5, 11, 12,  4, 15 },
               { 13,  8, 11, 5,  6, 15,  0,  3,  4, 7, 2, 12,  1, 10, 14,  9 },
               { 10,  6,  9, 0, 12, 11,  7, 13, 15, 1, 3, 14,  5,  2,  8,  4 },
               {  3, 15,  0, 6, 10,  1, 13,  8,  9, 4, 5, 11, 12,  7,  2, 14 } 
         };

         Byte[][] s5 = { /* S-Box 5 */
               {  2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13, 0, 14,  9 },
               { 14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3, 9,  8,  6 },
               {  4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6, 3,  0, 14 },
               { 11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10, 4,  5,  3 } 
         };

         Byte[][] s6 = { /* S-Box 6 */
               { 12,  1, 10, 15, 9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11 },
               { 10, 15,  4,  2, 7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8 },
               {  9, 14, 15,  5, 2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6 },
               {  4,  3,  2, 12, 9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13 } 
         };

         Byte[][] s7 = { /* S-Box 7 */
               {  4, 11,  2, 14, 15, 0,  8, 13,  3, 12, 9,  7,  5, 10, 6,  1 },
               { 13,  0, 11,  7,  4, 9,  1, 10, 14,  3, 5, 12,  2, 15, 8,  6 },
               {  1,  4, 11, 13, 12, 3,  7, 14, 10, 15, 6,  8,  0,  5, 9,  2 },
               {  6, 11, 13,  8,  1, 4, 10,  7,  9,  5, 0, 15, 14,  2, 3, 12 }
         };

         Byte[][] s8 = { /* S-Box 8 */
               { 13,  2,  8, 4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7 },
               {  1, 15, 13, 8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2 },
               {  7, 11,  4, 1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8 },
               {  2,  1, 14, 7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11 }
         };

         Byte[][][] boxRow = { s1, s2, s3, s4, s5, s6, s7, s8 };

         Byte[][] _4BitOutput = new Byte[8][4];
         for (int s = 0; s < _6BitInput.length; s++) {
            _4BitOutput[s] = boxSelect(_6BitInput[s], boxRow[s]);
         }

         Byte[] _32BitOutput = new Byte[32];

         byte index = 0;
         for (int s = 0; s < 8; s++) {
            for (int e = 0; e < 4; e++) {
               _32BitOutput[index] = _4BitOutput[s][e];
               index++;
            }
         }

         return _32BitOutput;
      }

      return null;
   }

   private static Byte[] P(Byte[] _32BitInput) {
      if (_32BitInput.length == 32) {
         Byte[] p = { 16,  7, 20, 21, 29, 12, 28, 17,
                       1, 15, 23, 26,  5, 18, 31, 10, 
                       2,  8, 24, 14, 32, 27,  3,  9, 
                      19, 13, 30,  6, 22, 11,  4, 25 };

         return permute(p, _32BitInput);
      }

      return null;
   }

   private static Byte[] PC1(Byte[] _64BitInput) {
      if (_64BitInput.length == 64) {
         Byte[] pc1 = { 57, 49, 41, 33, 25, 17,  9,  1, 
                        58, 50, 42, 34, 26, 18, 10,  2,
                        59, 51, 43, 35, 27, 19, 11,  3,
                        60, 52, 44, 36, 63, 55, 47, 39,
                        31, 23, 15,  7, 62, 54, 46, 38, 
                        30, 22, 14,  6, 61, 53, 45, 37, 
                        29, 21, 13,  5, 28, 20, 12,  4 };

         return permute(pc1, _64BitInput);
      }

      return null;
   }

   private static Byte[] PC2(Byte[] _56BitInput) {
      if (_56BitInput.length == 56) {
         Byte[] pc2 = { 14, 17, 11, 24,  1,  5,  3, 28, 
                        15,  6, 21, 10, 23, 19, 12,  4,
                        26,  8, 16,  7, 27, 20, 13,  2,
                        41, 52, 31, 37, 47, 55, 30, 40,
                        51, 45, 33, 48, 44, 49, 39, 56,
                        34, 53, 46, 42, 50, 36, 29, 32 };

         return permute(pc2, _56BitInput);
      }

      return null;
   }

   void print(Byte[] bitstring) {
      for (int i = 0; i < bitstring.length; i++) {
         System.out.print(bitstring[i].toString());
      }
      System.out.println();
   }
   
}