import java.io.UnsupportedEncodingException;

public class DESmain {

   public static void main(String[] args) throws UnsupportedEncodingException {
      String plaintextMessage = "Bachelor"; // 8 UTF-8 Zeichen bilden 64-Bit
      String hexKey = "50617373776f7274"; // 16 Hex-Zeichen (0-f) bilden 64-Bit

      DES des = new DES();

      Byte[] plaintextBits = des.plaintextToBin(plaintextMessage);
      Byte[] keyBits = des.keyToBin(hexKey);

      // Encryption
      Byte[] encryptedBits = des.encrypt(plaintextBits, keyBits);

      System.out.println("\n[=== Geheimtext ===]");
      System.out.printf("DESenc:\t");
      des.print(encryptedBits);

      System.out.println("\n********************\n");

      // Decryption
      Byte[] decryptedBits = des.decrypt(encryptedBits, keyBits);

      System.out.println("\n[=== Klartext ===]");
      System.out.printf("DESdec:\t");
      des.print(decryptedBits);
   }

}