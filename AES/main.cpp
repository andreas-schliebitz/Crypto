#include <cstdlib>

#include "AES.h"

int main(void) {
    constexpr mat4x4 S = {{
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 10, 11, 12},
        {13, 14, 15, 16}
    }};
    std::cout << "Plaintext:\n";
    printMat(S);
    
    const matNx4 K = {
        {15, 14, 13, 12},
        {11, 10, 9, 8},
        {7, 6, 5, 4},
        {3, 2, 1, 0},
        {255, 254, 253, 252},
        {251, 250, 249, 248},
        {247, 246, 245, 244},
        {243, 242, 241, 240},
    };
    std::cout << "Key:\n";
    printMat(K);

    AES aes(256);

    const mat4x4 C = aes.encrypt(S, K);
    std::cout << "Ciphertext:\n";
    printMat(C);

    const mat4x4 Q = aes.decrypt(C, K);
    std::cout << "Decrypted ciphertext (plaintext):\n";
    printMat(Q);
        
    return EXIT_SUCCESS;
}
