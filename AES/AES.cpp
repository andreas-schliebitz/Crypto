/*
 * Advanced Encryption Standard (AES)
 *
 * Author: Andreas Schliebitz
 * Date: 2020-06-07
 * License: GNU GPLv2
 */

#include "AES.h"

AES::AES(ushort Kl) {
    switch (Kl) {
        case 128:
            this->N = 10;
            break;
        case 192:
            this->N = 12;
            break;
        case 256:
            this->N = 14;
            break;
        default:
            std::cerr << "Invalid AES key size: " << Kl << " bits.\n";
            std::exit(1);
    }
    this->Kl = Kl;
}

void AES::setKey(const matNx4 &K) {
    if (K.size() * 32 != this->Kl) {
        std::cerr << "Provided key size and actual key size do not match.\n";
        std::exit(2);
    }
    this->K = K;
}
 
mat4x4 AES::encrypt(const mat4x4 &S, const matNx4 &K) {
    this->S = S;
    this->setKey(K);
    
    this->keySchedule();
    this->addRoundKey(0);

    for (byte r = 1; r <= this->N - 1; ++r) {
        this->subBytes(SB);
        this->shiftRows();
        this->mixColumns(MIX);
        this->addRoundKey(r);
    }

    this->subBytes(SB);
    this->shiftRows();
    this->addRoundKey(this->N);

    return this->S;
}

mat4x4 AES::decrypt(const mat4x4 &C, const matNx4 &K) {
    this->S = C;
    this->setKey(K);

    this->keySchedule();
    this->addRoundKey(this->N);

    for (byte r = this->N - 1; r >= 1; --r) {
        this->subBytes(SB_INV);
        this->invShiftRows();
        this->addRoundKey(r);
        this->mixColumns(MIX_INV);
    }

    this->subBytes(SB_INV);
    this->invShiftRows();
    this->addRoundKey(0);

    return this->S;
}

void AES::keySchedule() {
    const byte n = this->Kl / 32;
    for (byte i = 0; i < this->Ki.size(); ++i) {
        if (i < n) {
           this->Ki[i] = this->K[i];
        } else if (i >= n && i % n == 0) {
            this->Ki[i] = xorWord(xorWord(this->Ki[i - n], subWord(rotWord(this->Ki[i - 1]))), RCON[i / n]);
        } else if (i >= n && n > 6 && i % n == 4) {
            this->Ki[i] = xorWord(this->Ki[i - n], subWord(this->Ki[i - 1]));
        } else {
            this->Ki[i] = xorWord(this->Ki[i - n], this->Ki[i - 1]);
        }
    }
}

byte AES::multBytes(byte a, byte b) {
    byte p = 0;
    while (a && b) {
        if (b & 1)
            p ^= a;
        if (a & 0x80)
            a = (a << 1) ^ 0x11b;
        else
            a <<= 1;
        b >>= 1;
    }
    return p;
}

vec4 AES::rotWord(vec4 B) {
    std::rotate(B.begin(), B.begin() + 1, B.end());
    return B;
}

vec4 AES::subWord(vec4 B) {
    for (byte i = 0; i < 4; ++i)
        B[i] = sBox(B[i], SB);
    return B;
}

vec4 AES::xorWord(const vec4 &A, const vec4 &B) {
    vec4 X = {};
    for (byte i = 0; i < 4; ++i)
        X[i] = A[i] ^ B[i];
    return X;
}

byte AES::sBox(byte b, const mat16x16 &SB) {
    return SB[(b >> 4) & 0x0f][b & 0x0f];
}

void AES::subBytes(const mat16x16 &SB) {
    for (byte i = 0; i < 4; ++i)
        for (byte j = 0; j < 4; ++j)
            this->S[i][j] = sBox(this->S[i][j], SB);
}

void AES::shiftRows() {
    for (byte i = 0; i < 4; ++i)
        std::rotate(this->S[i].begin(), this->S[i].begin() + i, this->S[i].end());
}

void AES::invShiftRows() {
    for (byte i = 0; i < 4; ++i)
        std::rotate(this->S[i].rbegin(), this->S[i].rbegin() + i, this->S[i].rend());
}

void AES::mixColumns(const mat4x4 &MIX) {
    for (byte j = 0; j < 4; ++j) {
        const vec4 a = {this->S[0][j], this->S[1][j], this->S[2][j], this->S[3][j]};
        for (byte i = 0; i < 4; ++i) {
            this->S[i][j] = 0;
            for (byte k = 0; k < 4; ++k)
                this->S[i][j] ^= multBytes(MIX[i][k], a[k]);    
        }
    }
}

void AES::addRoundKey(byte r) {
    for (byte i = 0; i < 4; ++i)
        for (byte j = 0; j < 4; ++j)
            this->S[i][j] ^= this->Ki[4 * r][j];
}
