import java.math.BigInteger;

public final class RSAcrack {

	private BigInteger N;
	private static final BigInteger TWO = new BigInteger("2");

	public RSAcrack(BigInteger _N) {
		this.N = _N;
	}

	public BigInteger[] factorise() {
		if (this.N.mod(TWO).compareTo(BigInteger.ZERO) == 0)
			return new BigInteger[] { TWO, N.divide(TWO) };

		BigInteger i = new BigInteger("3");
		while (true) {
			if (this.N.mod(i).compareTo(BigInteger.ZERO) == 0)
				return new BigInteger[] { i, N.divide(i) };

			i = i.add(TWO);
		}
	}

	public static void main(String[] args) {
		RSAcrack obj = new RSAcrack(new BigInteger("5816324694156351731"));
		BigInteger[] factors = obj.factorise();

		for (BigInteger b : factors)
			System.out.println(b);
	}
}