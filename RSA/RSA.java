import java.math.BigInteger;
import java.util.ArrayList;

public final class RSA {

   final BigInteger e = new BigInteger("65537");
   BigInteger N, d;

   public RSA(BigInteger p, BigInteger q) {
      if (validGap(p, q)) {
         BigInteger phiN = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));

         if (BigInteger.ONE.compareTo(this.e) == -1 && this.e.compareTo(phiN) == -1) {
            this.N = p.multiply(q);
            this.d = this.e.modInverse(phiN);
         }
      }
   }

   BigInteger[] encrypt(short[] m) {
      ArrayList<BigInteger> c = new ArrayList<BigInteger>();

      for (short plaintext : m) {
         BigInteger cipher = this.enc(plaintext);
         c.add(cipher);
         System.out.printf("encrypt('%s' -> %d) = %d\n", (char) plaintext, plaintext, cipher);
      }

      return c.toArray(new BigInteger[c.size()]);
   }

   private BigInteger enc(Short m) {
      BigInteger mBig = new BigInteger(m.toString());
      return mBig.compareTo(this.N) == -1 ? mBig.modPow(this.e, this.N) : null;
   }

   Short[] decrypt(BigInteger c[]) {
      ArrayList<Short> mDec = new ArrayList<Short>();

      for (BigInteger cipher : c) {
         short plaintext = dec(cipher);
         mDec.add(plaintext);
         System.out.printf("decrypt(%d) = %d\t-> '%s'\n", cipher, plaintext, (char) plaintext);
      }

      return mDec.toArray(new Short[mDec.size()]);
   }

   private short dec(BigInteger c) {
      return c.modPow(this.d, this.N).shortValue();
   }

   short[] prepare(String s) {
      String[] chars = s.split("");

      short[] res = new short[chars.length];
      for (int i = 0; i < res.length; i++) {
         res[i] = (short) s.charAt(i);
      }

      return res;
   }

   private static boolean validGap(BigInteger p, BigInteger q) {
      double gap = Math.abs(log2(p) - log2(q));
      return 0.1 < gap && gap < 30;
   }

   private static double log2(BigInteger x) {
      int shiftCount = x.bitLength() - 64;

      if (shiftCount < 200) {
         return Math.log(x.doubleValue());
      }

      return Math.log(x.shiftRight(shiftCount).longValue()) + shiftCount * Math.log(2.0);
   }

}
