import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public class RSAmain {

   public static void main(String[] args) throws UnsupportedEncodingException {
      BigInteger p = new BigInteger("32416187719");
      BigInteger q = new BigInteger("179426549");

      RSA rsa = new RSA(p, q);
      String plaintextMessage = "Wissenschaftliches Arbeiten und Methoden (WAuM)";

      System.out.println("m = " + plaintextMessage);
      System.out.println("e = " + rsa.e);
      System.out.println("d = " + rsa.d);
      System.out.println("N = " + rsa.N);

      rsa.decrypt(rsa.encrypt(rsa.prepare(plaintextMessage)));
   }
   
}