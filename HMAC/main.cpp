#include "HMAC.h"

int main(void) {
    const vecN K = {0, 1, 2, 3, 4, 5, 6};
    std::cout << "Key (K):\n";
    print(K);
    
    const vecN M = {6, 5, 4, 3, 2, 1, 0};
    std::cout << "Message (M):\n";
    print(M);

    const vecN MAC = HMAC::digest(K, M);
    std::cout << "HMAC-SHA3-256(K, M):\n";
    print(MAC);

    return EXIT_SUCCESS;
}
