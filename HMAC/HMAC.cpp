#include "HMAC.h"

vecN HMAC::digest(const vecN &K, const vecN &M) {
    const vecN k = deriveKey(K);
    return sha3(cat(_xor(k, OPAD), sha3(cat(_xor(k, IPAD), M))));
}

vecN HMAC::deriveKey(vecN K) {
    if (K.size() > SHA3_256_BLOCKSIZE)
        K = sha3(K);
    if (K.size() < SHA3_256_BLOCKSIZE)
        K = zeroPad(K, SHA3_256_BLOCKSIZE);
    return K;
}

vecN HMAC::sha3(const vecN &x) {
    vecN h(SHA3_256_DIGESTSIZE);
    CryptoPP::SHA3_256 hash;
    hash.Update(x.data(), x.size());
    hash.Final(h.data());
    return h;
}

vecN HMAC::zeroPad(vecN x, ushort max) {
    if (x.size() > max) return x;
    while (max - x.size())
        x.emplace_back(0x00);
    return x;
}

vecN HMAC::_xor(const vecN &a, const vecN &b) {
    vecN x(a.size());
    for (std::size_t i = 0; i < a.size(); ++i)
        x[i] = a[i] ^ b[i];
    return x;
}

vecN HMAC::cat(vecN a, const vecN &b) {
    a.insert(a.end(), b.begin(), b.end());
    return a;
}
