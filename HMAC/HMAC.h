#ifndef HMAC_H
#define HMAC_H

#include <iostream>
#include <vector>
#include <string>
#include <cryptopp/sha3.h>

using byte = std::uint8_t;
using ushort = std::uint16_t;
using vecN = std::vector<byte>;

constexpr byte SHA3_256_BLOCKSIZE = 136;
constexpr byte SHA3_256_DIGESTSIZE = 32;

const vecN IPAD(SHA3_256_BLOCKSIZE, 0x36);
const vecN OPAD(SHA3_256_BLOCKSIZE, 0x50);

class HMAC {
    public:
        static vecN digest(const vecN &K, const vecN &M);
    
    private:
        static vecN deriveKey(vecN K);
        static vecN sha3(const vecN &x);
        static vecN zeroPad(vecN x, ushort max);
        static vecN _xor(const vecN &a, const vecN &b);
        static vecN cat(vecN a, const vecN &b);
};

template <typename T>
void print(const T &vec) {
    for (const auto &e : vec)
        std::cout << static_cast<unsigned>(e) << ' ';
    std::cout << '\n';
}

#endif /* HMAC_H */
